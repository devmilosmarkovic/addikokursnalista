//
//  Kurs.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import Foundation

struct Kurs {
    let zemlja: String
    let kup: String
    let pro: String
    let sre: String
}
