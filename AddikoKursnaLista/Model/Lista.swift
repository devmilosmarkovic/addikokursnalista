//
//  Lista.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import Foundation

struct Lista {
    let datumString: String
    let kursevi:[String:Kurs]
    
    init(datumString: String, lista: [String: Kurs]) {
        self.datumString = datumString
        self.kursevi = lista
    }
    
    init(jsonDictionary: NSDictionary) {
        var json = jsonDictionary as! Dictionary<String, Any>
        self.datumString = json["date"] as! String
        json.removeValue(forKey: "date")
        var lista: [String: Kurs] = [:]
        for (key, value) in json {
            let valueDict = value as! Dictionary<String, String>
            lista[key] = Kurs(zemlja: key, kup: valueDict["kup"]!, pro: valueDict["pro"]!, sre: valueDict["sre"]!)
        }
        self.kursevi = lista
    }
    
}
