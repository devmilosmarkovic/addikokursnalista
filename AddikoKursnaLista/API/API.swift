//
//  API.swift
//  AdikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import Foundation

struct API {
    enum APIError: String, Error {
        case InvalidURL = "Invalid URL for request"
        case NoResponseData = "No response data"
        case CannotSerializeToJSON = "Cannot serialize dictionary to JSON"
        case CannotResolveResult = "Cannot resolve result from JSON"
    }
    static let kExchangeRateFormat = "http://api.kursna-lista.info/%@/kursna_lista/json"
    static let kConvertFormat = "http://api.kursna-lista.info/%@/konvertor/%@/%@/%@/%@/sre/json"
    static let kApiKey = "52e743fa7d9cb4b00aac1a280ff21bdf"
    static let kTimeout = 15.0
    static let kResultKey = "result"
    
    static func getRequest(requestURL: URL, completion:@escaping (NSDictionary) -> (), errorHandler: @escaping (Error)->()) {
        var request = URLRequest(url: requestURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: API.kTimeout)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let err = error {
                errorHandler(err)
                return
            }
            guard let responseData = data else {
                errorHandler(APIError.NoResponseData)
                return
            }
            //let debugResponseString = String.init(data: responseData, encoding: .utf8)
            //debugPrint(debugResponseString ?? "Cannot resolve response string")
            do {
                guard let convertedJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? NSDictionary else {
                    errorHandler(APIError.CannotSerializeToJSON)
                    return
                }
                if let result = convertedJSON.object(forKey: API.kResultKey) as? NSDictionary {
                    completion(result)
                } else {
                    errorHandler(APIError.CannotResolveResult)
                }
            }
            catch let error as NSError {
                errorHandler(error)
            }
        }
        task.resume()
    }
    //Dohvata kursnu listu
    static func getExchangeRates(completion:@escaping (NSDictionary) -> (), errorHandler: @escaping (Error)->()) {
        let requestString = String(format: API.kExchangeRateFormat, arguments: [API.kApiKey])
        guard let url = URL(string: requestString) else {
            errorHandler(APIError.InvalidURL)
            return
        }
        self.getRequest(requestURL: url, completion: completion, errorHandler: errorHandler)
    }
    //konverzija
    static func convert(izValute: String, uValutu: String, iznos: String, datum: String, completion:@escaping (NSDictionary) -> (), errorHandler: @escaping (Error)->()) {
        let requestString = String(format: API.kConvertFormat, arguments: [API.kApiKey, izValute, uValutu, iznos, datum])
        guard let url = URL(string: requestString) else {
            errorHandler(APIError.InvalidURL)
            return
        }
        self.getRequest(requestURL: url, completion: completion, errorHandler: errorHandler)
    }
}
