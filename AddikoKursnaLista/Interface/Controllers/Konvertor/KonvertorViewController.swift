//
//  KonvertorViewController.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import UIKit

class KonvertorViewController: UIViewController {
    static let animationDuration = 0.3
    
    var valute: [String]!
    var datum: String!
    var lastSavedAmount: Double = 0.0
    var formatter: NumberFormatter!
    var izValute: String!
    var uValutu: String!
    var buttonRequestedChange: UIButton?
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var izValuteButton: UIButton!
    @IBOutlet weak var uValutuButton: UIButton!
    @IBOutlet weak var zahtevButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //format
        self.formatter = NumberFormatter.init()
        self.formatter.numberStyle = .decimal
        self.formatter.allowsFloats = true
        self.formatter.minimumFractionDigits = 2
        self.formatter.minimumIntegerDigits = 1
        self.formatter.alwaysShowsDecimalSeparator = true
        self.formatter.minimum = NSNumber(value: 0.1)
        //valute
        self.valute.append("rsd")
        self.valute.sort()
        
        self.izValute = "rsd"
        self.uValutu = "eur"
        //dugmici
        self.setupButtonText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hidePanel()
    }
    
    func setupButtonText() {
        self.izValuteButton.setTitle(self.izValute.uppercased(), for: .normal)
        self.uValutuButton.setTitle(self.uValutu.uppercased(), for: .normal)
    }
    
    @IBAction func changeFromTapped(_ sender: Any) {
        self.buttonRequestedChange = sender as? UIButton
        self.showPanel()
    }
    
    @IBAction func changeToTapped(_ sender: Any) {
        self.buttonRequestedChange = sender as? UIButton
        self.showPanel()
    }
    
    @IBAction func okTapped(_ sender: Any) {
        self.hidePanel()
        let row = self.pickerView.selectedRow(inComponent: 0)
        let text = self.valute[row]
        if let button = self.buttonRequestedChange {
            if button == self.izValuteButton {
                self.izValute = text
            } else {
                self.uValutu = text
            }
        }
        self.setupButtonText()
    }
    
    @IBAction func zahtevTapped(_ sender: Any) {
        self.zahtevButton.isEnabled = false
        let iznos = self.textField.text!
        DispatchQueue.global(qos: .background).async {
            API.convert(izValute: self.izValute, uValutu: self.uValutu, iznos: iznos, datum: self.datum, completion: { (dictionary) in
                let value = dictionary["value"] as! String
                let message = String(format: "Po kursu od %@, %@ %@ iznosi %@ %@", arguments:[self.datum, iznos, self.izValute.uppercased(), self.uValutu.uppercased(), value])
                let alertCtr = UIAlertController.init(title: "Obaveštenje", message: message, preferredStyle: .alert)
                alertCtr.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                DispatchQueue.main.async {
                    self.present(alertCtr, animated: true, completion: nil)
                    self.zahtevButton.isEnabled = true
                }
            }) { (error) in
                debugPrint(error.localizedDescription)
                let alertCtr = UIAlertController.init(title: "Desila se greška", message: error.localizedDescription, preferredStyle:
                .alert)
                alertCtr.addAction(UIAlertAction(title:"Ok", style: .cancel, handler: nil))
                DispatchQueue.main.async {
                    self.present(alertCtr, animated: true, completion: nil)
                    self.zahtevButton.isEnabled = true
                }
            }
        }
        
    }
    
    @IBAction func backgroundTap(_ sender: Any) {
        debugPrint("background tap")
        self.textField.endEditing(false)
    }
    
    func showPanel() {
        self.okButton.isHidden = false
        self.pickerView.isHidden = false
        UIView.animate(withDuration: KonvertorViewController.animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hidePanel() {
        self.okButton.isHidden = true
        self.pickerView.isHidden = true
        UIView.animate(withDuration: KonvertorViewController.animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension KonvertorViewController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        defer {
            textField.text = self.formatter.string(from: NSNumber(value: self.lastSavedAmount))
        }
        guard let text = textField.text, let value: Double = self.formatter.number(from: text)?.doubleValue else {
            return
        }
        self.lastSavedAmount = value
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        debugPrint("should return")
        textField.endEditing(false)
        return true
    }
}

extension KonvertorViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.valute.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = self.valute[row]
        return title
    }
    
}
