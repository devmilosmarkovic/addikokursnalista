//
//  KursTableViewController.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import UIKit

class KursTableViewController: UITableViewController {
    static let KursCellIdentifier = "KursCellIdentifier"
    static let SegueKonvertorToLista = "SegueKonvertorToLista"
    
    @IBOutlet weak var konvertorButton: UIBarButtonItem!
    
    var model: KursTableViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadModel), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        self.model = KursTableViewModel(delegate: self)
        self.reloadModel()
    }
    // MARK: - Reloading
    @objc func reloadModel() {
        self.tableView.refreshControl?.beginRefreshing()
        self.konvertorButton.isEnabled = false
        self.model?.load()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if model?.cellInfos != nil {
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model?.cellInfos?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: KursTableViewCell = tableView.dequeueReusableCell(withIdentifier: KursTableViewController.KursCellIdentifier, for: indexPath) as! KursTableViewCell
        let info = self.model!.cellInfos![indexPath.row]
        cell.slika.image = info.slika
        cell.kupovni.text = info.kupovni
        cell.prodajni.text = info.prodajni
        cell.srednji.text = info.srednji
        cell.zemlja.text = info.zemlja
        return cell
    }

    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == KursTableViewController.SegueKonvertorToLista {
            guard let model = self.model, let _ = model.lista else {
                return false
            }
        }
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == KursTableViewController.SegueKonvertorToLista {
            if let dest = segue.destination as? KonvertorViewController {
                dest.valute = self.model!.sveValute()
                dest.datum = self.model!.lista!.datumString
            }
        }
    }
}

extension KursTableViewController: KursTableViewModelDelegate {
    func didReload() {
        self.tableView.refreshControl?.endRefreshing()
        self.konvertorButton.isEnabled = true
        self.tableView.reloadData()
    }
}
