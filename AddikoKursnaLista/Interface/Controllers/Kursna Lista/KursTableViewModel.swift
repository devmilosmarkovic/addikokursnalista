//
//  KursTableViewModel.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import UIKit

protocol KursTableViewModelDelegate {
    func didReload()
}

struct CellInfo {
    let slika: UIImage?
    let kupovni: String
    let prodajni: String
    let srednji: String
    let zemlja: String
}

final class KursTableViewModel {
    var lista: Lista?
    var cellInfos: [CellInfo]?
    var delegate: KursTableViewModelDelegate?
    
    init(delegate: KursTableViewModelDelegate?) {
        self.delegate = delegate
    }
    
    func load() {
        DispatchQueue.global(qos: .background).async {
            API.getExchangeRates(completion: { (jsonDictionary) in
                self.lista = Lista.init(jsonDictionary: jsonDictionary)
                self.loadCellInfos()
            }) { (error) in
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func loadCellInfos() {
        self.cellInfos = self.lista!.kursevi.map { (key, value) -> CellInfo in
            let kup = (value.kup == "") ? "-" : value.kup
            let pro = (value.pro == "") ? "-" : value.pro
            let sre = (value.sre == "") ? "-" : value.sre
            return CellInfo(slika: UIImage.init(named: key),
                            kupovni: kup,
                            prodajni: pro,
                            srednji: sre,
                            zemlja: key.uppercased())
            }.sorted(by: { (info1, info2) -> Bool in
                info1.zemlja < info2.zemlja
            })
        DispatchQueue.main.async {
             self.delegate?.didReload()
        }
    }
    
    func sveValute() -> [String] {
        return self.lista!.kursevi.keys.sorted()
    }
}
