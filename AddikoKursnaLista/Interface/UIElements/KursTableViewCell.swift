//
//  KursTableViewCell.swift
//  AddikoKursnaLista
//
//  Created by Markovic Milos on 4/17/18.
//  Copyright © 2018 milosmarkovic. All rights reserved.
//

import UIKit

class KursTableViewCell: UITableViewCell {
    @IBOutlet weak var slika: UIImageView!
    @IBOutlet weak var kupovni: UILabel!
    @IBOutlet weak var prodajni: UILabel!
    @IBOutlet weak var srednji: UILabel!
    @IBOutlet weak var zemlja: UILabel!
}
